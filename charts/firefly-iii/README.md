<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://firefly-iii.org/">
    <img src="https://fireflyiiiwebsite.z6.web.core.windows.net/assets/logo/small.png" alt="Firefly III" width="120" height="178">
  </a>
</p>
  <h1 align="center">Firefly III</h1>

  <p align="center">
    A free and open source personal finance manager
  </p>

This Helm chart installs [Firefly-III](https://github.com/firefly-iii/firefly-iii), and a MySQL backend. Optionally, an external MySQL or PostgreSQL backend may be specified. A [CSV importer](https://github.com/firefly-iii/csv-importer) can be enabled to import transactions.

_Note: This chart is not maintained by the upstream project. Any issues with the chart should be raised [here](https://gitlab.com/sddan/charts)_

## Prerequisites

- Kubernetes 1.16+
- Helm 3+

## Get Repo Info

```console
helm repo add sddan https://sddan.gitlab.io/charts
helm repo update
```

## Install Chart

```console
# Helm
$ helm install [RELEASE_NAME] sddan/firefly-iii
```

_See [configuration](#configuration) below._

_See [helm install](https://helm.sh/docs/helm/helm_install/) for command documentation._

## Uninstall Chart

```console
# Helm
$ helm uninstall [RELEASE_NAME]
```

This removes all the Kubernetes components associated with the chart and deletes the release.

_See [helm uninstall](https://helm.sh/docs/helm/helm_uninstall/) for command documentation._

## Upgrading Chart

```console
# Helm
$ helm upgrade [RELEASE_NAME] sddan/firefly-iii
```

_See [helm upgrade](https://helm.sh/docs/helm/helm_upgrade/) for command documentation._

### Upgrading an existing Release to a new major version

A major chart version change (like v0.1.0 -> v1.0.0) indicates that there is an incompatible breaking change needing manual actions.

## Configuration

See [Customizing the Chart Before Installing](https://helm.sh/docs/intro/using_helm/#customizing-the-chart-before-installing). To see all configurable options with detailed comments:

```console
helm show values sddan/firefly-iii
```

### Multiple releases

The same chart can be used to run multiple Firefly-III instances in the same cluster if required. To achieve this, only the Firefly-III frontend component needs to be enabled. The same MySQL backend may be used for multiple releases, with each release using a different database within the first release's MySQL deployment (or external DB, if specified).
